import React, { useEffect, useState } from "react";
import { Table } from "./components/Table";
import { Header } from "./components/Header";

import { Cat } from "./interface";
import { randomName } from "./services/randomName";
import { randomAge } from "./services/randomAge";
import { randomColor } from "./services/randomColor";
import { randomCollar } from "./services/randomCollar";
import { constants } from "./constants";

let uniqueId = 1;

export const App = () => {
	const [state, setState] = useState<Cat[]>([]);

	const createCat = () => {
		const newCat = {
			id: uniqueId,
			name: randomName(),
			age: randomAge(),
			color: randomColor(),
			collar: randomCollar(),
			isHungry: false,
		};

		setState((prev) => [...prev, newCat]);
		hungry(newCat);

		uniqueId++;
	};

	const feed = (cat: Cat) => {
		setState((prev) =>
			prev.map((item) => {
				if (item.id === cat.id) return { ...item, isHungry: false };
				else return item;
			})
		);
	};

	const hungry = (cat: Cat) => {
		setInterval(() => {
			setTimeout(() => {
				setState((prev) => prev.filter((item) => !(item.id === cat.id && item.isHungry)));
			}, constants.catSpamTime);

			setState((prev) =>
				prev.map((item) => {
					if (item.id === cat.id) {
						return { ...item, isHungry: true };
					} else return item;
				})
			);
		}, constants.catGetsHungryTime);
	};

	useEffect(() => {
		setInterval(createCat, constants.catSpamTime);
	}, []);

	return (
		<>
			<Header />

			<div className="wrapper">
				<div>
					<h3 className="table__title">Cats</h3>
					<Table
						columns={[
							{
								title: "Cat name",
								dataKey: "name",
								render: (value: any) => value,
							},
							{
								title: "Color",
								dataKey: "color",
								render: (value: any) => value,
							},
							{
								title: "Hungry",
								dataKey: "isHungry",
								render: (value: any, values: Cat) =>
									value ? <button onClick={() => feed(values)}>feed</button> : "",
							},
						]}
						items={state.filter((item) => item.collar)}
					/>
				</div>

				<div>
					<h3 className="table__title">Neighbour cats</h3>
					<Table
						columns={[
							{
								title: "Cat name",
								dataKey: "name",
								render: (value: any) => value,
							},
							{
								title: "Color",
								dataKey: "color",
								render: (value: any) => value,
							},
							{
								title: "Hungry",
								dataKey: "isHungry",
								render: (value: any, values: Cat) =>
									value ? <button onClick={() => feed(values)}>feed</button> : "",
							},
						]}
						items={state.filter((item) => !item.collar)}
					/>
				</div>
			</div>
		</>
	);
};
