// import React from "react";
// import { Table } from "./components/Table";
// import { Header } from "./components/Header";
//
// import { IAppState, Cat } from "./interface";
// import { randomName } from "./services/randomName";
// import { randomAge } from "./services/randomAge";
// import { randomColor } from "./services/randomColor";
// import { randomCollar } from "./services/randomCollar";
// import { constants } from "./constants";
// import { inArray } from "./utils/inArray";
//
// let uniqueId = 1;
//
// export class App extends React.Component {
// 	state: IAppState = {
// 		allCats: [],
// 		neighboursCats: [],
// 	};
//
// 	constructor(props: any) {
// 		super(props);
// 		this.createCat = this.createCat.bind(this);
// 		this.checkHungry = this.checkHungry.bind(this);
// 		this.feed = this.feed.bind(this);
// 	}
//
// 	createCat() {
// 		const newAllCats = [...this.state.allCats];
// 		const newNeighbourCats = [...this.state.neighboursCats];
//
// 		const newCat = {
// 			id: uniqueId,
// 			name: randomName(),
// 			age: randomAge(),
// 			color: randomColor(),
// 			collar: randomCollar(),
// 			isHungry: false,
// 		};
//
// 		if (newCat.collar) {
// 			newAllCats.push(newCat);
// 			this.setState({ allCats: newAllCats });
// 		} else {
// 			newNeighbourCats.push(newCat);
// 			this.setState({ neighboursCats: newNeighbourCats });
// 		}
//
// 		this.checkHungry(newCat, newNeighbourCats);
// 		uniqueId++;
// 	}
//
// 	checkHungry(cat: Cat, neighboursCats: Cat[]) {
// 		if (inArray(neighboursCats, cat.id)) {
// 			this._updateCats("neighboursCats", cat);
// 		} else {
// 			this._updateCats("allCats", cat);
// 		}
// 	}
//
// 	feed(cat: Cat, entity: keyof IAppState) {
// 		const newCats = this.state[entity].map((item) => {
// 			if (item.id === cat.id) {
// 				return { ...item, isHungry: false };
// 			} else return item;
// 		});
//
// 		this.setState({ [entity]: newCats });
// 	}
//
// 	_updateCats(entity: keyof IAppState, cat: Cat) {
// 		setInterval(() => {
// 			const newCats = this.state[entity].map((item) => {
// 				if (item.id === cat.id) {
// 					return { ...item, isHungry: true };
// 				} else return item;
// 			});
//
// 			setTimeout(() => {
// 				let savedIndex;
// 				const foundCat = this.state[entity].find((item, index) => {
// 					if (item.id === cat.id) savedIndex = index;
// 					return item.id === cat.id;
// 				});
//
// 				if (foundCat?.isHungry) {
// 					const newCats = [...this.state[entity]];
// 					if (savedIndex) newCats.splice(savedIndex, 1);
// 					this.setState({ [entity]: newCats });
// 				}
// 			}, constants.catSpamTime);
//
// 			this.setState({ [entity]: newCats });
// 		}, constants.catGetsHungryTime);
// 	}
//
// 	componentDidMount() {
// 		setInterval(this.createCat, constants.catSpamTime);
// 	}
//
// 	render() {
// 		const { neighboursCats, allCats } = this.state;
//
// 		return (
// 			<>
// 				<Header />
//
// 				<div className="wrapper">
// 					<div>
// 						<h3 className="table__title">Cats</h3>
// 						<Table
// 							columns={[
// 								{
// 									title: "Cat name",
// 									dataKey: "name",
// 									render: (value: any) => value,
// 								},
// 								{
// 									title: "Color",
// 									dataKey: "color",
// 									render: (value: any) => value,
// 								},
// 								{
// 									title: "Hungry",
// 									dataKey: "isHungry",
// 									render: (value: any, values: Cat) =>
// 										value ? (
// 											<button onClick={() => this.feed(values, "allCats")}>
// 												feed
// 											</button>
// 										) : (
// 											""
// 										),
// 								},
// 							]}
// 							items={allCats}
// 						/>
// 					</div>
//
// 					<div>
// 						<h3 className="table__title">Neighbour cats</h3>
// 						<Table
// 							columns={[
// 								{
// 									title: "Cat name",
// 									dataKey: "name",
// 									render: (value: any) => value,
// 								},
// 								{
// 									title: "Color",
// 									dataKey: "color",
// 									render: (value: any) => value,
// 								},
// 								{
// 									title: "Hungry",
// 									dataKey: "isHungry",
// 									render: (value: any, values: Cat) =>
// 										value ? (
// 											<button
// 												onClick={() => this.feed(values, "neighboursCats")}
// 											>
// 												feed
// 											</button>
// 										) : (
// 											""
// 										),
// 								},
// 							]}
// 							items={neighboursCats}
// 						/>
// 					</div>
// 				</div>
// 			</>
// 		);
// 	}
// }

export const a = {};
