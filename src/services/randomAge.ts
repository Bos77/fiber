const ages: number[] = [1, 2, 3, 4, 5, 6, 7, 8];

export const randomAge = (): number => {
	const randomIndex = Math.round(Math.random() * (ages.length - 1));

	return ages[randomIndex];
};
