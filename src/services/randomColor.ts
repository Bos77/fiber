const colors: string[] = [
	"Red",
	"Blue",
	"Black",
	"Brown",
	"Black-Brown",
	"Black-White",
	"White-Orange",
	"Orange",
];

export const randomColor = (): string => {
	const randomIndex = Math.round(Math.random() * (colors.length - 1));

	return colors[randomIndex];
};
