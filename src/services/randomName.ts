const names: string[] = [
	"Molly",
	"Felix",
	"Smudge",
	"Sooty",
	"Tigger",
	"Charlie",
	"Alfie",
	"Oscar",
];

export const randomName = (): string => {
	const randomIndex = Math.round(Math.random() * (names.length - 1));

	return names[randomIndex];
};
