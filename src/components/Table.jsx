import React from "react";

import { TableHead } from "./TableHead";
import { TableRow } from "./TableRow";

import "./Table.scss";

export const Table = ({ items = [], columns = [] }) => {
	return (
		<table className="table">
			<TableHead columns={columns} />

			<tbody>
				{items.length ? (
					items.map((row, index) => <TableRow key={index} row={row} columns={columns} />)
				) : (
					<tr>
						<td>No Data</td>
					</tr>
				)}
			</tbody>
		</table>
	);
};
