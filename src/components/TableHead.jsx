import React from "react";

export const TableHead = ({ columns }) => {
	return (
		<thead>
			<tr>
				{columns.map((col, index) => (
					<th key={index} className="table__th">
						{col.title}
					</th>
				))}
			</tr>
		</thead>
	);
};
