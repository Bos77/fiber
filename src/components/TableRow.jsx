import React from "react";

export const TableRow = ({ columns, row }) => {
	return (
		<tr className="main-table-tr">
			{columns.map((col, innerIndex) => {
				return (
					<td key={innerIndex} className="table__td">
						{col.render(row[col.dataKey], row)}
					</td>
				);
			})}
		</tr>
	);
};
