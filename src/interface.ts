export type Cat = {
	id: number;
	name: string;
	age: number;
	color: string;
	collar: boolean;
	isHungry: boolean;
};

export type updateFn = (cats: Cat[]) => {};
