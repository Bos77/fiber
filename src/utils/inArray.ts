export const inArray = <T extends { id: number }>(array: T[], id: number) => {
	return array.find((item) => item.id && item.id === id);
};
